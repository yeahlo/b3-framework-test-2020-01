import React, {useState, useEffect, useContext} from 'react';
import './App.scss';
import {Store, updateData} from "./store";

const App = props => {

  const {state, dispatch} = useContext(Store);

  useEffect(() => {

    fetch('/data/data.json')
        .then(raw => raw.json())
        .then(json => dispatch(updateData(json)));
  },[]);

  useEffect(() => {
    console.log('data',state.data)
  }, [state.data]);

  return (
    <div className="App">
      {state.data.map(ingredient => <img src={ingredient.image} />)}
    </div>
  );
};

export default App;
