import React, {useReducer} from 'react'

const UPDATE_DATA = 'UPDATE_DATA';

const initialState = {
    data: [],
};

export const updateData = (payload) => ({
    type: UPDATE_DATA,
    payload
});


const reducer = (state, action) => {

    switch (action.type) {
        case UPDATE_DATA :
            return {
                ...state,
                data : action.payload
            };
        default:
            return state;
    }
};

const Store = React.createContext();

const StoreProvider = props => {
    const [state, dispatch] = useReducer(reducer, {...initialState});
    const value = {state, dispatch};

    return <Store.Provider value={value}>{props.children}</Store.Provider>;
};

export {Store, StoreProvider};
